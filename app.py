# Python script to control raspberry pi (wemo clone) via alexa device

import fauxmo
import logging
import time
import sys
import subprocess,shlex

# import file from directory
#import plex_wol

from debounce_handler import debounce_handler

#LOGGINE IN LOG FILE
logging.basicConfig(format='%(asctime)s %(message)s',filename='./logs/app_logs.log',level=logging.DEBUG)

#LOGGIN IN TERMINAL
#logging.basicConfig(level=logging.DEBUG)

class device_handler(debounce_handler):
    """Publishes the on/off state requested,
       and the IP address of the Echo making the request.
    """

    TRIGGERS = {"Plex Server": 52000, "Theater TV":52001}

    def act(self, client_address, state, name):
        if(name == "Plex Server"):
            if(state == True):

                try:
                    proc = subprocess.call(["./plex-control/plex_wol.sh"])
                    logging.debug("%s STATE:ON >> from client @%s" % (name,client_address))

                except:
                    logging.debug("Plex Turn On Process TIMEOUT")
                    proc.kill()
                    
            else:
                logging.debug("FALSE STATE NOT HANDLED YET")

        
        elif(name == "Theater TV"):
              if(state == True):

                  try:
                      proc = subprocess.call(["./tv-control/turnOn.sh"])
                      logging.debug("%s STATE:ON >> from client @%s" % (name,client_address))

                  except TimoutExpired:
                      logging.debug("Theater TV Turn On Process TIMOUT")
                      proc.kill()
                      
              else:
                  try:

                     proc = subprocess.call(["./tv-control/turnOff.sh"])
                     logging.debug("%s STATE:OFF >> from client @%s" % (name,client_address))

                  except TimeoutExpired:
                      logging.debug("TV Turn off Process TIMEOUT")
                      proc.kill()
        return True


if __name__ == "__main__":
    # Startup the fauxmo server
    fauxmo.DEBUG = True
    p = fauxmo.poller()
    u = fauxmo.upnp_broadcast_responder()
    u.init_socket()
    p.add(u)

    # Register the device callback as a fauxmo handler
    d = device_handler()
    for trig, port in d.TRIGGERS.items():
        fauxmo.fauxmo(trig, u, p, None, port, d)

    # Loop and poll for incoming Echo requests
    logging.debug("Entering fauxmo polling loop")
    while True:
        try:
            # Allow time for a ctrl-c to stop the process
            p.poll(100)
            time.sleep(0.1)
        except Exception, e:
            logging.critical("Critical exception: " + str(e))
            break