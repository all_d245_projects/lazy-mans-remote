# Lazy Mans Remote

simple python script which enables the user to send a WOL packet to an internet connected device (in this case a compatible smart TV and a PLEX media server) to turn on and off the device.

inspiration from [instructables](http://www.instructables.com/id/Hacking-the-Amazon-Echo/)